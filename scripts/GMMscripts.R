
################################################################################
#
#   The contents of this file are subject to the CeCILL v2.1 license. You may 
#   not use this file except in compliance with the license terms.
#
#   The original code is developed by SCALIAN DS.
#   Portions of code created by SCALIAN DS are Copyright (C) SCALIAN DS.
#   All right reserved.
#
#   SCALIAN DS
#   417 L'Occitane - CS 77679, 31676 Labege Cedex  - France
#   Phone: +33 (0)5 61 00 79 79     
#   http://www.scalian.com
#
################################################################################

GM.latent.Gen <- function(N,pi,seed=12){
  
  ######################### Latent class serie generation ##########################
  #INPUT:
    # N: sample size
    # Pi: K classes probabilities
    # seed: random state for reproductibility
  
  #OUTPUT: generated atent K classes serie Z
  set.seed(seed)
  
  Z = sapply(1:N,function(t)which(rmultinom(1,1,pi)==1))
  
  return(Z)
}

GM.Gen <- function(N,Z,mu,sigma2,M=1,seed=12){
  
  ######################### Gaussian mixture ensemble sample generation ##########################
  #INPUT:
    # N: sample size
    # Z: latent K classes serie
    # mu: K means vector (or matrix d>1)
    # sigma2: K variance vector (or list of covariance matrix d>1)
    # M: ensemble size
    # seed: random state for reproductibility
  #OUTPUT: generated gaussian ensemble sample (N,M,d) following the latent K classes serie Z
    
  
  set.seed(seed)
  if(is.null(dim(mu))){
    d = 1
  }else{
    d = dim(mu)[2]
  }

  Y = array(0,dim=c(N,M,d))
  K = length(levels(as.factor(Z)))
  #state loop
  library(mvtnorm)
  for(k in 1:K){
    if(d==1){
      Y[which(Z==k),,] =  t(replicate(length(which(Z==k)),rnorm(M,mean = mu[k],sd=sqrt(sigma2[k]))))
    }else{
      tmp = replicate(length(which(Z==k)),t(rmvnorm(M,mean = c(mu[k,]),sigma=as.matrix(sigma2[[k]]))))
      for(p in 1:d) Y[which(Z==k),,p] =  tmp[p,,]
    }
  }
  detach(package:mvtnorm)
  return(Y)
}



init.hc <- function(X,K,B=50,B.perc=1,treshold=100){
  
  ######################### Initialisation hierarchic clustering mclust ##########################
  #INPUT
    # X: data array (N,M,d)
    # K: cluster number
    # B: boostrap iteration
    # B.perc: percent of individu to pick for the bootstrap susbet
    # treshold: maximum sample size allowed for hierrachic clustering without bootstraping procedure.
    # seed: random state for reproductibility
    
  # OUTPUT:  list of results
    # theta.G: list of fitted parameters for initialise EM algorithm
      # cluster: serie of size N with K cluster (if onlyparams is false)
      # mu: mean vector of size K (or matrix K*d)
      # sigma2: variance vector of size K (or list of K covariance matrix d*d)
      # pi: class proportion vector of size K
    # sample: sample used for the best initialisation
    # initial.model: list of model fitted on B bootstrap iteration
  
  library(mclust)
  
  
  # Estimation of empiricals statistics following M
  N = dim(X)[1]
  M = dim(X)[2]
  d = dim(X)[3]
  
  # save true data
  Y_star = X
  
  if(M>1){
    # estimation of empirical statistics
    X_emp = sapply(1:d,function(d_)apply(X[,,d_],1,mean))
    X_var = sapply(1:d,function(d_)apply(X[,,d_],1,var))
    dim_stats = dim(X)
    # dim_stats[2] = 2
    X = array(0,dim=c(N,1,d*2))
    for(d_ in 1:d){
      X[,1,d_] = X_emp[,d_]
      X[,1,d+d_] = X_var[,d_]
    }
    
    M_old = M
    d_old=d
    N = dim(X)[1]
    M = dim(X)[2]
    d = dim(X)[3]
  }
  
  # Boostraping procedure for large sample size
  if(N>treshold){
    
    # B subsample selection
    init.sample = lapply(1:B,function(b){
      set.seed(b)
      s = sample(1:N,round(treshold*B.perc))
      return(s)
    })
    
    # B piors estimation
    init.model = lapply(1:B,function(b){
      set.seed(b)
      S <- init.sample[[b]]
      
      Y_star_S = array(Y_star[S,,],dim=c(length(S),dim(Y_star)[2],dim(Y_star)[3]))
      
      X_S = X[S,1,]
      
      # Hierarchical clustering
      out = hc(X_S)
      
      cl <- hclass(out,c(K))
      
      # Prior estimation based on true data Y_star with estimated class series fitted on empirical statistics
      if(d_old>1){
        X_S = Y_star_S
        init.tmp= list(mu_G =  t(as.matrix(sapply(1:K,function(k){
          ind = which(cl==k)
          if(length(ind)==1){
            return(apply(X_S[ind,,],2,mean))
          }else{
            return(colMeans(apply(X_S[cl==k,,],c(1,3),mean)))
          }}))),
          sigma2_G = lapply(1:K,function(k){
            ind = which(cl==k)
            if(length(ind)==0){
              return(matrix(0,d_old,d_old))
            }
            if(length(ind)>=1){
              ind = which(cl==k)
              sigma = 0
              for(l in ind){
                sigma = sigma+cov(X_S[l,,])
              }
              sigma = sigma/length(ind)
              return(sigma)
            }}),
          pi_k_G =  sapply(1:K,function(k)sum(cl==k)/dim(X_S)[1]),
          cluster_G=cl)
        
      }else{
        X_S = Y_star_S
        init.tmp= list(mu_G =  as.matrix(sapply(1:K,function(k){
          ind = which(cl==k)
          if(length(ind)>1){
            return(mean(apply(X_S[cl==k,,1],1,mean)))
          }else{
            return(mean(X_S[cl==k,,1]))
          }})),
          sigma2_G = sapply(1:K,function(k){
            ind = which(cl==k)
            if(length(ind)==0){
              return(0)
            }
            if(length(ind)==1){
              return(var(X_S[cl==k,,1]))
            }
            if(length(ind)>0){
              return(mean(apply(X_S[cl==k,,1],1,var)))
            }}),
          pi_k_G =  sapply(1:K,function(k)sum(cl==k)/dim(X_S)[1]),
          cluster_G=cl)
      }
        
      d=d_old
      X=Y_star
      
      # In case of empty class, penalise loglikelihood
      if(d>1){
        if(sum(init.tmp$pi_k_G==0)>0){
          loglikelihood = -Inf
        }else{
          loglikelihood = sum(log(rowSums(sapply(1:K,function(k){
            return(init.tmp$pi_k_G[k]*colprods(t(sapply(1:M,function(m)
              dmvnorm(X[,m,],mean = c(init.tmp$mu_G[k,]),sigma=as.matrix(init.tmp$sigma2_G[[k]]))))))
          }))))
        }
      }else{
        if(sum(init.tmp$pi_k_G==0)>0){
          loglikelihood = -Inf
        }else{
          loglikelihood = sum(log(rowSums(sapply(1:K,function(k){
            return(init.tmp$pi_k_G[k]*colprods(t(sapply(1:M,function(m)
              dnorm(X[,m,],mean = init.tmp$mu_G[k],sd=sqrt(init.tmp$sigma2_G[k]))))))
          }))))
        }
      }
      return(list(init.tmp=init.tmp,loglikelihood=loglikelihood))
    })
    
    # Selection of the b prior maximising the Gaussian mixture loglikelihood
    ind = which.max(sapply(init.model,function(b.model)b.model$loglikelihood))
    init.model[['Sample']] = init.sample[[ind]]
    init.tmp = init.model[[ind]]$init.tmp

    d=d_old
    X=Y_star

  }else{
    
    # Hierarchical clustering
    X_tmp = X[,1,]
    
    out = hc(X_tmp)
    
    
    cl <- hclass(out,c(K))
    
    # Prior estimation based on Y_star with estimated class series fitted on empirical statistics
    if(d_old>1){
      X_S = Y_star
      init.tmp= list(mu_G =  t(as.matrix(sapply(1:K,function(k){
        ind = which(cl==k)
        if(length(ind)==1){
          return(apply(X_S[ind,,],2,mean))
        }else{
          return(colMeans(apply(X_S[cl==k,,],c(1,3),mean)))
        }}))),
        sigma2_G = lapply(1:K,function(k){
          ind = which(cl==k)
          if(length(ind)==0){
            return(matrix(0,d_old,d_old))
          }
          if(length(ind)>=1){
            ind = which(cl==k)
            sigma = 0
            for(l in ind){
              sigma = sigma+cov(X_S[l,,])
            }
            sigma = sigma/length(ind)
            return(sigma)
          }}),
        pi_k_G =  sapply(1:K,function(k)sum(cl==k)/dim(X_S)[1]),
        cluster_G=cl)
      
    }else{
      X_S = Y_star
      init.tmp= list(mu_G =  as.matrix(sapply(1:K,function(k){
        ind = which(cl==k)
        if(length(ind)>1){
          return(mean(apply(X_S[cl==k,,1],1,mean)))
        }else{
          return(mean(X_S[cl==k,,1]))
        }})),
        sigma2_G = sapply(1:K,function(k){
          ind = which(cl==k)
          if(length(ind)==0){
            return(0)
          }
          if(length(ind)==1){
            return(var(X_S[cl==k,,1]))
          }
          if(length(ind)>0){
            return(mean(apply(X_S[cl==k,,1],1,var)))
          }}),
        pi_k_G =  sapply(1:K,function(k)sum(cl==k)/dim(X_S)[1]),
        cluster_G=cl)
    }
    d=d_old
    X=Y_star
    
    
    init.model= init.tmp
  }

  # Save prior estimates and loglikelihood
  init.model = init.model
  if(d>1){
    loglikelihood = sum(log(rowSums(sapply(1:K,function(k){
      if(det(init.tmp$sigma2_G[[k]])<1e-10){
        return(0)
      }else{
        return(init.tmp$pi_k_G[k]*colprods(t(sapply(1:M,function(m)
          dmvnorm(X[,m,],mean = c(init.tmp$mu_G[k,]),sigma=as.matrix(init.tmp$sigma2_G[[k]]))))))
      }}))))
  }else{
    loglikelihood = sum(log(rowSums(sapply(1:K,function(k){
      if(init.tmp$sigma2_G[k]==0){
        return(0)
      }else{
        return(init.tmp$pi_k_G[k]*colprods(t(sapply(1:M,function(m)
          dnorm(X[,m,],mean = init.tmp$mu_G[k],sd=sqrt(init.tmp$sigma2_G[k]))))))
      }}))))
  }
  detach(package:mclust)
  return(list(init.tmp=init.tmp,init.model=init.model,loglikelihood=loglikelihood))
}


gmm <- function(X, K, model,seed=12,
                init='hc',opts=list('B'=50,'B.perc'=1,'treshold'=100,'eps'=1e-5,'itermax'=5,'verbose'=FALSE,
                                    'iter.res'=FALSE,'itermax'=30,'onlyParams'=FALSE)){
  
  ######################### Main function of GM models for ensemble data set ##########################
  #INPUT:
    # X: data array (N,M,d)
    # K: cluster number
    # model: model name (Empirical statistics, Super Sample, Exchangeable variables)
    # init: initial procedure used to estimate prior for EM or list of initial parameters 
    #        to provide in the ouput format. For Empirical statistics, Super Sample models the sample serie is require.
    # opts: list of options model
    # B.perc: percent of individu to pick for the bootstrap susbet
    # treshold: maximum sample size allowed for hierrachic clustering without bootstraping procedure.
    # seed: random state for reproductibility
  
  #OUTPUT:
    # list of results: model estimated parameters in gmm.em output format:
        # theta: list of fitted parameters
          # softcluster: gamma probabilities matrix (N*K) (if onlyparams is false)
          # cluster: serie of size N with K cluster (if onlyparams is false)
          # mu: mean vector of size K (or matrix K*d)
          # sigma2: variance vector of size K (or list of K covariance matrix d*d)
          # pi: class proportion vector of size K
  
  init.model=NULL
  N = dim(X)[1]
  M = dim(X)[2]
  d = dim(X)[3]
  
  # Estimate empirical statistics from the ensemble
  if(model=='Empirical statistics'){
    
    if(M>1){
      X_emp = apply(X,c(1,3),mean)
      X_var = apply(X,c(1,3),var)
      X = cbind(X_emp,X_var)
    }
    N = dim(X)[1]
    d = dim(X)[2]
  }
  
  # Transform the data set (N,M,d) in a new shape (N*M,d)
  if(model=='Super Sample'){
    
    
    X = array(X,dim=c(N*M,d))
    
    N = dim(X)[1]
    d = dim(X)[2]
  }
  if((length(init)!=0)&(init!='hc')){
    init.tmp = init$init.tmp
    init.model = init$init.model
  }else{
    init.tmp=NULL
  }
  
  
  flag = 0
  if((model%in%c('Exchangeable variables'))&(M>1)){
    if(init=='hc'){
      init.list <- init.hc(X,K,B=opts$B,B.perc=opts$B.perc,treshold=opts$treshold)
      init.tmp = init.list$init.tmp
      init.model = init.list$init.tmp
    }
    modelgm <- gmm.em(X, K,eps=opts$eps,
                      seed=seed,
                      init=init.tmp,
                      verbose=opts$verbose,
                      iter.res=opts$iter.res,
                      itermax=opts$itermax,
                      onlyParams=opts$onlyParams)
      
    
  }else{
    flag=1
  }
  if((model%in%c('Empirical statistics','Super Sample'))|(flag==1)){
    library(mclust)
    if(length(init.tmp)==0){
      
      BICsel = list(try(mclustBIC(X, G = K,verbose=FALSE),silent = TRUE))
      out = list(try(mclust::Mclust(X,G = K,verbose=FALSE),silent = TRUE))
    }else{
      S = init.tmp$sample
      BICsel = list(try(mclustBIC(X, G = K,verbose=FALSE,initialization = list(subset = S)),silent = TRUE))
      out = list(try(mclust::Mclust(X,G=K,verbose=FALSE,initialization = list(subset = S)),silent = TRUE))
        
    }

    model_tmp <- out[[1]]

    if(d>1){
      modelgm =list(theta = list(mu = t(model_tmp$parameters$mean),
                                 sigma2=lapply(1:K,function(k)model_tmp$parameters$variance$sigma[,,k]),
                                 pi=model_tmp$parameters$pro,
                                 cluster=model_tmp$classification),
                    BIC=apply(-BICsel[[1]],1,min,na.rm=T))
    }else{
      modelgm =list(theta = list(mu = model_tmp$parameters$mean,
                                 sigma2=model_tmp$parameters$variance$sigmasq,
                                 pi=model_tmp$parameters$pro,
                                 cluster=model_tmp$classification),
                    BIC=apply(-BICsel[[1]],1,min,na.rm=T))
    }
    detach(package:mclust)
  }

  modelgm[[init]] = init.model
  return(modelgm)
}


gmm.em <- function(X, K,eps=1e-5,seed=12,init=NA,verbose=FALSE,iter.res=FALSE,
                   itermax=30,onlyParams=FALSE){
  
  ######################### EM for gaussian mixture model with exchangeable variable ##########################
  #INPUT:
    # X: data array (N,M,d)
    # K: cluster number
    # eps: EM stop criterion convergence 
    # init: initialisation parameters
    # verbose: print loglikelihood at each iteration
    # iter.res: save iteration parameters
    # itermax: maximum EM iteration
    # onlyParams: save only parameters and not cluster serie
    # seed: random state for reproductibility
  
  #OUTPUT:
    # list of results:
      # theta.G: list of inital parameters
          # cluster: serie of size N with K cluster (if onlyparams is false)
          # mu: mean vector of size K (or matrix K*d)
          # sigma2: variance vector of size K (or list of K covariance matrix d*d)
          # pi: class proportion vector of size K
          
      # theta: list of fitted parameters
          # softcluster: gamma probabilities matrix (N*K) (if onlyparams is false)
          # cluster: serie of size N with K cluster (if onlyparams is false)
          # mu: mean vector of size K (or matrix K*d)
          # sigma2: variance vector of size K (or list of K covariance matrix d*d)
          # pi: class proportion vector of size K
  
      # seed: 
      # BIC: Baysian information criterion
      # ICL: Integrated Complete-data Likelihood criterion
      # iter.res: list of fitted parameters at each iteration (if iter.res is true)

  
  N <- dim(X)[1]  # Number of realizations
  M <- dim(X)[2]  # Size of sample
  d <- dim(X)[3]  # Realization dimension
  Delta <- 1
  iter <- 0
  converged = EM_converged(0, 2 * eps, eps)
  loglikelihood_mem = -Inf
  list.iter.res = c()
  flag=1
  X.tmp = X
  
  # START EM
  while(converged[1] == 0 && iter <= itermax){
    
    # INITIALISE PARAMETERS
    if((iter == 0)){
      mu_mem = init$mu_G
      mu = init$mu_G
      mu_G = init$mu_G
      sigma2 = init$sigma2_G
      Sigma2_G = init$sigma2_G
      pi_k_G = init$pi_k_G
      pi_k = init$pi_k_G
      cluster_G = init$cluster_G
      
      sigma2_mem = sigma2
      pi_k_mem = pi_k
      
      if(d==1){
        mu_mem = t(mu)
        mu = t(mu)
      }

    }
    X=X.tmp
    N = dim(X.tmp)[1]
    
    if(d==1){
      #########################################################
      ################### Univariate Case   ###################
      #########################################################
      
      ################### E-step ###################

      ###### Estimate gamma probabilities 
      P_Xt_Zt <- sapply(1:K,function(k){
        if(sigma2[k]!=0){
          return(pi_k[k]*colprods(t(sapply(1:M,function(m)
            dnorm(X[,m,],mean = mu[k],sd=sqrt(sigma2[k]))))))
        }else{
          return(replicate(dim(X)[1],0))
        }})
      P_Xt <- rowSums(P_Xt_Zt)
      
      # Ban outliers from estimation
      ind_t = which(P_Xt == 0)
      if(length(which(P_Xt == 0))>0){
        ind_t = which(P_Xt == 0)
        X = array(X[-ind_t,,],dim=c(N-length(ind_t),M,d))
        P_Xt_Zt = P_Xt_Zt[-ind_t,]
        P_Xt = P_Xt[-ind_t]
        N = N-length(ind_t)
        
        X = array(X,dim=c(N,M,d))
        
        ind_t = NA
        flag=0
      }
      
      # Break if there are too much banned elements
      if(dim(X)[1]<=1){
        sigma2 = sigma2_mem
        mu = mu_mem
        pi_k = pi_k_mem
        break
      }

      # Assignement                           
      gamma <- P_Xt_Zt / P_Xt
      
      Z = apply(gamma, 1,which.max)

      # Compute loglikelihood of the gaussian mixture model with exchangeable variables
      loglikelihood = sum(log(rowSums(sapply(1:K,function(k){
        if(sum(sigma2[k])!=0){
          return(pi_k[k]*colprods(t(sapply(1:M,function(m)
            dnorm(X[,m,],mean = mu[k],sd=sqrt(sigma2[k]))))))
        }else{
          return(replicate(dim(X)[1],0))
        }}))))
      
      # Check convergence
      converged = EM_converged(loglikelihood, loglikelihood_mem, eps,verbose=verbose)
      loglikelihood_mem = loglikelihood

      # Stop in case of deacreasing loglikelihood or empty cluster
      ind = sapply(1:K,function(k)sigma2[k]==0)

      if((converged$decrease==1)|(sum(ind)!=0)){
        sigma2 = sigma2_mem
        mu = mu_mem
        pi_k = pi_k_mem
        break
      }
      
      # save last params
      mu_mem = mu
      sigma2_mem = sigma2
      pi_k_mem = pi_k
      ################### M-step ###################
      N_k <- colSums(gamma,na.rm=T)
      pi_k <- N_k/sum(N_k)
      mu <- t(sapply(1:K, function(k) sapply(1:d,function(d_){
        if(N_k[k] >  1){
          return(1/(M*N_k[k]) * colSums(gamma[, k] * as.matrix(apply(as.matrix(X[,,d_]),1,sum))))
        }else{
          return(mu[k])
        }
      })))
      sigma2 <- t(sapply(1:K, function(k){
        if(N_k[k] >  1){
          return(1/(M*N_k[k]) * colSums(gamma[, k] * as.matrix(apply((as.matrix(X[,,d])-mu[,k])^2,1,sum))))
        }else{
          return(0)
        }}))
      
      
    }else{

      #########################################################
      ################### Multivariate Case ###################
      #########################################################
      
      ################### E-step ###################
      
      ###### Estimate gamma probabilities 
      P_Xt_Zt <- sapply(1:K,function(k){
        out = try(chol(sigma2[[k]]),silent=TRUE)
        if(typeof(out)=='double'){
          return(pi_k[k]*colprods(t(sapply(1:M,function(m)
            dmvnorm(X[,m,],mu = c(mu[k,]),sigma=as.matrix(sigma2[[k]]))))))
        }else{
          return(replicate(dim(X)[1],0))
        }
      })
      
      P_Xt <- rowSums(P_Xt_Zt)
      
      # Ban outliers from estimation
      ind_t = which(P_Xt == 0)
      if(length(which(P_Xt == 0))>0){
        ind_t = which(P_Xt == 0)
        X = X[-ind_t,,]
        P_Xt_Zt = P_Xt_Zt[-ind_t,]
        P_Xt = P_Xt[-ind_t]
        N = N-length(ind_t)
        X = array(X,dim=c(N,M,d))
        
        ind_t = NA
        flag=0
      }
      
      # Break if there are too much banned elements
      if(dim(X)[1]<=1){
        sigma2 = sigma2_mem
        mu = mu_mem
        pi_k = pi_k_mem
        break
      }
      # Assignement                                             
      gamma <- P_Xt_Zt / P_Xt
      
      Z = apply(gamma, 1,which.max)
      
      # Compute loglikelihood of the gaussian mixture model with exchangeable variables
      loglikelihood = sum(log(rowSums(sapply(1:K,function(k){
          out = try(chol(sigma2[[k]]),silent=TRUE)
          if(typeof(out)=='double'){
            return(pi_k[k]*colprods(t(sapply(1:M,function(m)
              dmvnorm(X[,m,],mu = c(mu[k,]),sigma=as.matrix(sigma2[[k]]))))))
          }else{
            return(replicate(dim(X)[1],0))
          }
        }))))
      
      
      # Check convergence
      converged = EM_converged(loglikelihood, loglikelihood_mem, eps,verbose=verbose)
      loglikelihood_mem = loglikelihood
      
      
      # Stop in case of deacreasing loglikelihood or empty cluster
      ind = sapply(1:K,function(k){
        out = try(chol(sigma2[[k]]),silent=TRUE)
        return((typeof(out)!='double'))
      })
      
      if((converged$decrease==1)|(sum(ind)!=0)){
        sigma2 = sigma2_mem
        mu = mu_mem
        pi_k = pi_k_mem
        break
      }

      # Save last params
      mu_mem = mu
      sigma2_mem = sigma2
      pi_k_mem = pi_k
      ################### M-step ###################
      N_k <- colSums(gamma,na.rm=T)
      pi_k <- N_k/sum(N_k)
      
      if(M>1){
        mu <- t(sapply(1:K, function(k) sapply(1:d,function(d_){
          return(1/(M*N_k[k]) * colSums(gamma[, k] * as.matrix(apply(X[,,d_],1,sum))))})))
        sigma2 <- lapply(1:K,function(k){
          if(N_k[k] >= 1){
            c = 1/(M*N_k[k])
            t = 1
            Sigma_tmp = c*gamma[t, k]*(t(X[t,,])-mu[k,])%*%t(t(X[t,,])-mu[k,])
            for(t in 2:N){
              Sigma_tmp = Sigma_tmp+c*gamma[t, k]*(t(X[t,,])-mu[k,])%*%t(t(X[t,,])-mu[k,])
            }
          }else{
            if(N_k[k] == 1){
              t = which(Z==k)
              c = 1/(M*N_k[k])
              return(c*gamma[t, k]*(t(X[t,,])-mu[k,])%*%t(t(X[t,,])-mu[k,]))
            }else{
              Sigma_tmp = matrix(0,d,d)
            }
          }
          return(Sigma_tmp)})
      }else{
        mu <- t(sapply(1:K, function(k) sapply(1:d,function(d_)
          if(N_k[k] > 1){
            return(1/(M*N_k[k]) * colSums(gamma[, k] * as.matrix(X[,N,d_])))
          }else{
            return(1/(M*1) * colSums(gamma[, k] * as.matrix(X[,N,d_])))
          })))
        
        sigma2 <- lapply(1:K,function(k){
          if(N_k[k] > 1){
            c = 1/(M*N_k[k])
            mu_mat =t(replicate(N,mu[k,]))
            Sigma_tmp = c*(t(gamma[, k]*as.matrix(X[,1,]-mu_mat))%*%(as.matrix(X[,1,]-mu_mat)))
          }
          else{
            return(matrix(0,d,d)) 
          }
          return(Sigma_tmp)})
      }
    }
    iter <- iter + 1
    
    # Print loglikelihood
    if(verbose){
      message(paste0("Loglikelihood := ",loglikelihood))
    }
    
    # Save parameters at each iteration
    if(iter.res){
      if(onlyParams){
        list.iter.res = c(list.iter.res,list(list(loglikelihood=loglikelihood,
                                                  mu=mu,
                                                  sigma2=sigma2,
                                                  pi=pi_k)))
      }else{
        list.iter.res = c(list.iter.res,list(list(loglikelihood=loglikelihood,
                                                  mu=mu,
                                                  sigma2=sigma2,
                                                  pi=pi_k,
                                                  cluster=Z)))
      }
    }
  }

  X=X.tmp
  N = dim(X.tmp)[1]
  
  # Reassignement and final loglikelihood computation step
  if(d==1){
    ################### Univariate Case   ###################
    #########################################################
    
    ################### E-step ###################
    
    ###### Estimate gamma probabilities 
    P_Xt_Zt <- sapply(1:K,function(k){
      if(sigma2[k]!=0){
        return(pi_k[k]*colprods(t(sapply(1:M,function(m)
          dnorm(X[,m,],mean = mu[k],sd=sqrt(sigma2[k]))))))
      }else{
        return(replicate(dim(X)[1],0))
      }})
    P_Xt <- rowSums(P_Xt_Zt)
    
    
    # Check outlier and reassigne them by local renormalisation or k-means procedure
    ind_t = which(P_Xt == 0)
    if(length(which(P_Xt == 0))>0){
      ind_t = which(P_Xt == 0)
      
      # local renormalisation
      for(t in ind_t){
        P_Xt_Zt[t,] =sapply(1:K, function(k){
          if(sigma2[k]!=0){
            return(pi_k[k]*prod(dnorm(X[t,,],mean = c(mu[k]),sd=sqrt(sigma2[k]))
                                /max(dnorm(X[t,,],mean = c(mu[k]),sd=sqrt(sigma2[k])))))
          }else{
            return(0)
          }})
        
        P_Xt[t] =sum(P_Xt_Zt[t,])
      }
      
      # k-means procedure if renormalisation is not enough
      if(length(which(P_Xt == 0))>0){
        ind_t = which(P_Xt == 0)
        dist_ <- sapply(1:K, function(k) sapply(ind_t, 
                                                function(t)sum(mu[k] - apply(as.matrix(X[t,,]),2,mean))^2))
        if(length(ind_t)==1){
          dist_ = t(as.matrix(dist_))
        }
        dist_ = t(apply(dist_,1,function(vec)vec/max(vec)))
        P_Xt_Zt[ind_t,] = dist_
        P_Xt[ind_t]=1
        flag=1
      }
    }

    # Assignement                           
    gamma <- P_Xt_Zt / P_Xt
    
    Z = apply(gamma, 1,which.max)
    
    
    loglikelihood = sum(log(rowSums(sapply(1:K,function(k){
      if(sum(sigma2[k])!=0){
        return(pi_k[k]*colprods(t(sapply(1:M,function(m)
          dnorm(X[,m,],mean = mu[k],sd=sqrt(sigma2[k]))))))
      }else{
        return(replicate(dim(X)[1],0))
      }}))))
    
  }else{
    ################### Multivariate Case ###################
    #########################################################
    
    
    ################### E-step ###################
    
    ###### Estimate gamma probabilities 
    K = dim(mu)[1]
    P_Xt_Zt <- sapply(1:K,function(k){
      
      out = try(chol(sigma2[[k]]),silent=TRUE)
      if(typeof(out)=='double'){
        return(pi_k[k]*colprods(t(sapply(1:M,function(m)
          dmvnorm(X[,m,],mu = c(mu[k,]),sigma=as.matrix(sigma2[[k]]))))))
      }else{
        return(replicate(dim(X)[1],0))
      }
      })
    
    P_Xt <- rowSums(P_Xt_Zt)
    
    # Check outlier and reassigne them by local renormalisation or k-means procedure
    ind_t = which(P_Xt == 0)
    if(length(which(P_Xt == 0))>0){
      ind_t = which(P_Xt == 0)
      
      # local renormalisation
      for(t in ind_t){
        
        P_Xt_Zt[t,]=sapply(1:K, function(k){
          out = try(chol(sigma2[[k]]),silent=TRUE)
          if(typeof(out)=='double'){
            return(pi_k[k]*prod(dmvnorm(X[t,,],mu = c(mu[k,]),sigma=as.matrix(sigma2[[k]]))
                                /max(dmvnorm(X[t,,],mu = c(mu[k,]),sigma=as.matrix(sigma2[[k]])))))
          }else{
            return(0)
          }})
        P_Xt[t] =sum(P_Xt_Zt[t,])
        
      }
      
      # k-means procedure if renormalisation is not enough
      if(length(which(P_Xt == 0))>0){
        ind_t = which(P_Xt == 0)
        
        dist_ <- sapply(1:M, function(k) sapply(ind_t, 
                                                function(t)sum(mu[k,] - apply(as.matrix(X[t,,]),2,mean))^2))
        
        if(length(ind_t)==1){
          dist_ = t(as.matrix(dist_))
        }
        dist_ = t(apply(dist_,1,function(vec)vec/max(vec)))
        P_Xt_Zt[ind_t,] = dist_
        P_Xt[ind_t]=1
        flag=1
      }
    }
    
    # Assignement                                             
    gamma <- P_Xt_Zt / P_Xt
    Z = apply(gamma, 1,which.max)
    
    
    loglikelihood = sum(log(rowSums(sapply(1:K,function(k){
        out = try(chol(sigma2[[k]]),silent=TRUE)
        if(typeof(out)=='double'){
          return(pi_k[k]*colprods(t(sapply(1:M,function(m)
            dmvnorm(X[,m,],mu = c(mu[k,]),sigma=as.matrix(sigma2[[k]]))))))
        }else{
          return(replicate(dim(X)[1],0))
        }
      }))))
    
  }

  
  # Reorder parameters by increasing variance
  index =  sort(sapply(1:K,function(k){if(d>1){tmp = sigma2[[k]]
  }else{tmp = sigma2[k]
  }
    return(sum(diag(as.matrix(tmp))))}), decreasing = FALSE,index.return=T)$ix
  gamma = gamma[,index]
  Z = apply(gamma, 1,which.max)
  pi_k = pi_k[index]
  mu_tmp = mu
  mu = array(0,dim=c(K,d))
  mu[1:K,] = mu_tmp
  mu = mu[index,]
  sigma2 = sigma2[index]
  
  # Compute Baysian information criterion
  BIC = K*(1+1*d+1*d*d)*log(N*M)-2*loglikelihood
  
  for(k in 1:dim(gamma)[2]){
    gamma[gamma[,k]==0,k] = 3.579729e-205
  }
  
  
  # Compute Integrated Complete-data Likelihood criterion
  ICL = BIC-2*sum(apply(gamma,1,function(p){sum(p*log(p))}))
  
  # Save results
  if(onlyParams){
    res = list(theta.G = list(mu=mu_G,sigma2=Sigma2_G,pi=pi_k_G),
               theta = list(mu=mu,sigma2=sigma2,pi=pi_k),
               seed=seed,
               BIC = BIC,
               ICL = ICL)
    
    if(iter.res){
      res = list(theta.G = list(mu=mu_G,sigma2=Sigma2_G,pi=pi_k_G),
                 theta = list(mu=mu,sigma2=sigma2,pi=pi_k),
                 seed=seed,
                 BIC = BIC,
                 ICL = ICL,
                 iter.res=list.iter.res)
    }
  }else{
    res = list(theta.G = list(cluster = cluster_G, mu=mu_G,sigma2=Sigma2_G,pi=pi_k_G),
               theta = list(softcluster = gamma, cluster = Z, mu=mu,sigma2=sigma2,pi=pi_k),
               seed=seed,
               BIC = BIC,
               ICL = ICL)
    
    if(iter.res){
      res = list(theta.G = list(cluster = cluster_G, mu=mu_G,sigma2=Sigma2_G,pi=pi_k_G),
                 theta = list(softcluster = gamma, cluster = Z, mu=mu,sigma2=sigma2,pi=pi_k),
                 seed=seed,
                 BIC = BIC,
                 ICL = ICL,
                 iter.res=list.iter.res)
    }
    if((length(ind_t)>0)|(flag==0)){
      
      clust.predict = gm.predict(res,X.tmp,'Exchangeable variables')
      
      res$theta$softcluster = clust.predict$softcluster
      res$theta$cluster = clust.predict$cluster
    }
  }
  
  
  
  return(res)
}


gm.predict <- function(model,X,modelname){
  ######################### CLUSTER PREDICTION FOR GM MODELS WITH ENSEMBLE DATA ##########################
  #INPUT:
    # model: gmm models with fitted parameters
    # X: data array (N,M,d)
    # modelname: model name (Empirical statistics, Super Sample, Exchangeable variables)
  
  #OUTPUT:
   # list of results:
      # cluster: predicted serie of size N of K cluster
      # sofclust: gamma probabilities matrice (N*K)

  N <- dim(X)[1]
  M <- dim(X)[2] 
  d <- dim(X)[3]

  # Estimate empirical statistics from the ensemble
  if(modelname%in%c('Empirical statistics')){
    if(M>1){
      X_emp = sapply(1:d,function(d_)apply(X[,,d_],1,mean))
      X_var = sapply(1:d,function(d_)apply(X[,,d_],1,var))
      X = array(0,dim=c(N,1,d*2))
      for(d_ in 1:d){
        X[,1,d_] = X_emp[,d_]
        X[,1,d+d_] = X_var[,d_]
      }
    }
  }
  N = dim(X)[1]
  M = dim(X)[2]
  d = dim(X)[3]
  
  # Get model parameters
  mu = model$theta$mu
  sigma2 = model$theta$sigma2
  pi_k = model$theta$pi
  
  
  # Transform the data set (N,M,d) in a new shape (N*M,d)
  if(modelname=='Super Sample'){
    old_N=N
    old_M=M
    old_d=d
    
    X = array(X,dim=c(N*M,d))
    
    N = dim(X)[1]
    d = dim(X)[2]
    
  }

  if(modelname%in%c('Empirical statistics','Exchangeable variables')){
    if(d==1){
      # Estimate gamma for univariate case with model prameters
      K = length(mu)
      P_Xt_Zt <- sapply(1:K,function(k){
        if(sigma2[k]!=0){
          return(pi_k[k]*colprods(t(sapply(1:M,function(m)
            dnorm(X[,m,],mean = mu[k],sd=sqrt(sigma2[k]))))))
        }else{
          return(replicate(dim(X)[1],0))
        }})
      P_Xt <- rowSums(P_Xt_Zt)
      
    }else{
      K = dim(mu)[1]
      P_Xt_Zt <- sapply(1:K,function(k){
        out = try(chol(sigma2[[k]]),silent=TRUE)
        if(typeof(out)=='double'){
          return(pi_k[k]*colprods(t(sapply(1:M,function(m)
            dmvnorm(X[,m,],mu = c(mu[k,]),sigma=as.matrix(sigma2[[k]]))))))
        }else{
          return(replicate(dim(X)[1],0))
        }})
      
      P_Xt <- rowSums(P_Xt_Zt)
    }
    
    # In case of outliers, local renormalisation step
    ind_t = which(P_Xt == 0)
    if(length(which(P_Xt == 0))>0){
      ind_t = which(P_Xt == 0)
      for(t in ind_t){
        if(d==1){
          P_Xt[t] =sum(sapply(1:K, function(k)pi_k[k]*prod(dnorm(X[t,,],mean = c(mu[k]),sd=sqrt(sigma2[k]))
                                                           /max(dnorm(X[t,,],mean = c(mu[k]),sd=sqrt(sigma2[k]))))))
          P_Xt_Zt[t,] =(sapply(1:K, function(k)pi_k[k]*prod(dnorm(X[t,,],mean = c(mu[k]),sd=sqrt(sigma2[k]))
                                                            /max(dnorm(X[t,,],mean = c(mu[k]),sd=sqrt(sigma2[k]))))))
          
          
        }else{
          P_Xt[t] =sum(sapply(1:K, function(k)pi_k[k]*prod(dmvnorm(X[t,,],mu = c(mu[k,]),sigma=as.matrix(sigma2[[k]]))
                                                           /max(dmvnorm(X[t,,],mu = c(mu[k,]),sigma=as.matrix(sigma2[[k]]))))))
          P_Xt_Zt[t,] =(sapply(1:K, function(k)pi_k[k]*prod(dmvnorm(X[t,,],mu = c(mu[k,]),sigma=as.matrix(sigma2[[k]]))
                                                            /max(dmvnorm(X[t,,],mu = c(mu[k,]),sigma=as.matrix(sigma2[[k]]))))))
          
        }
        
      }
      
      # If local renormalisation not enough, k-means procedure
      if(length(which(P_Xt == 0))>0){
        ind_t = which(P_Xt == 0)
        if(d==1){
          dist_ <- sapply(1:K, function(k) sapply(ind_t, 
                                                  function(t)sum(mu[k] - apply(as.matrix(X[t,,]),2,mean))^2))
        }else{
          dist_ <- sapply(1:K, function(k) sapply(ind_t, 
                                                  function(t)sum(mu[k,] - apply(as.matrix(X[t,,]),2,mean))^2))
        }
        if(length(ind_t)==1){
          dist_ = t(as.matrix(dist_))
        }
        dist_ = t(apply(dist_,1,function(vec)vec/max(vec)))
        P_Xt_Zt[ind_t,] = dist_
        P_Xt[ind_t]=1
        flag=1
      }
    }
    
    # Assignement                                             
    gamma <- P_Xt_Zt / P_Xt
    Z = apply(gamma, 1,which.max)
  }
  if(modelname%in%c('Super Sample')){
    
    # Estimate gamma for univariate case with model prameters
    if(d==1){
      K = length(mu)
      P_Xt_Zt = sapply(1:K,function(k){
        pi_k[k]*dnorm(X,mean = mu[k],sd=sqrt(sigma2[k]))
      })
      P_Xt = rowSums(P_Xt_Zt)
      gamma = P_Xt_Zt/P_Xt
      
    }else{
      K = dim(mu)[1]
      P_Xt_Zt = sapply(1:K,function(k){
        out = try(chol(sigma2[[k]]),silent=TRUE)
        if(typeof(out)=='double'){
          return(pi_k[k]*dmvnorm(X,mu = c(mu[k,]),sigma=as.matrix(sigma2[[k]])))
        }else{
          return(replicate(dim(X)[1],0))
        }})
      P_Xt = rowSums(P_Xt_Zt)
      gamma = P_Xt_Zt/P_Xt
      
    }
    gamma = array(gamma,dim=c(old_N,old_M,K))
    gamma = apply(gamma,c(1,3),mean)
    
    Z = apply(gamma, 1,which.max)
  }
  return(list(cluster=Z,softcluster=gamma))
}





EM_converged <- function(loglik, previous_loglik, threshold = 1e-5,verbose=FALSE) {
  ######################### CHECK CONVERGENCE OF EM ALGORITHM ##########################
  #INPUT:
    # loglik: obtained loglikelihood from the current EM iteration
    # previous_loglik: obtained loglikelihood from the previous EM iteration
    # threshold: EM performance to reach for stop
    # verbose: print logelikelihood
    
  #OUTPUT:
    # list of results:
      # converged: factor indicating EM converged to the set threshold
      # decrease: factor indicating a decrease of current loglikelihood compared to the previous EM iteration
  
  converged = 0;
  decrease = 0;
  if(!(previous_loglik==-Inf)){
    if (loglik - previous_loglik < -1e-2) # allow for a little imprecision 
    {
      if(verbose){
        message(paste("******likelihood decreased from ",previous_loglik," to ", loglik,sep=""))
      }
      decrease = 1;
    }
    
    delta_loglik = abs(loglik - previous_loglik);
    avg_loglik = (abs(loglik) + abs(previous_loglik) + threshold)/2;
    if (abs(delta_loglik)==Inf | abs(avg_loglik)==Inf){bb=0}
    else{bb = ((delta_loglik/avg_loglik) < threshold)}
    if (bb) {converged = 1}
  }
  
  res <- NULL
  res$converged <- converged
  res$decrease <- decrease
  return(res)
  
}




g_legend<-function(a.gplot){
  ######################### GRAPHICAL FUNCTION FOR LEGEND ADJUSTEMENT ##########################
  #INPUT:
    # a.gplot: ggplot graphical object
  #OUTPUT:
    # legend: ggplot graphical legend
  tmp <- ggplot_gtable(ggplot_build(a.gplot))
  leg <- which(sapply(tmp$grobs, function(x) x$name) == "guide-box")
  legend <- tmp$grobs[[leg]]
  return(legend)}
