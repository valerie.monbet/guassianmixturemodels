# Gaussian mixture models for clustering and calibration of ensemble weather forecasts

In this document, we proposed a simulation implementation of methods described on the [ref article] with ensemble data designed from gaussian mixture ![](https://render.githubusercontent.com/render/math?math=X).

To remind the main topic of [ref article], we provide clustering method to fit ensemble data ![](https://render.githubusercontent.com/render/math?math=X_{i}=(x_{i1},\cdots,x_{iM})+,+\forall+i+\in\\{1,...,n\\}) (M being the ensemble size and n the sample size) designed from a mixture model witk K gaussian components ![](https://render.githubusercontent.com/render/math?math=\mathcal{N}(\mu_k,\Sigma_k)+,+\forall+k+\in\\{1,...,K\\}). Ensemble data ![](https://render.githubusercontent.com/render/math?math=(x_{11},\cdots,x_{nM})) of ![](https://render.githubusercontent.com/render/math?math=n{\times}M{\times}d) can be similar to observe one different view of one of the K gaussian distribution ![](https://render.githubusercontent.com/render/math?math=X) at each i as illustrated in the figure 1.

<div class="image-wrapper" >
    <img src="/figures/example.png" alt=""/>
  </a>
      <p class="image-caption">Figure 1: Clustering objectiv with a bivariate ensemble data into K=3 classes.</p>
</div>

In the article, we presented and discussed about three differents methods used to fit ensemble data with the EM algorithm: Empirical statistics, Super sample and Exchangeable variable. Each of these methods is implemented on the following code 'GMMscripts.R' and described as below.

## Empirical statistics: 
The idea is to link the empirical statistics data estimated on ensemble ![](https://render.githubusercontent.com/render/math?math=S_{i}=(\overline{x}_i,s^2_i)) to the latent variable ![](https://render.githubusercontent.com/render/math?math=Z_{i}) charaterising the ensemble data designed by a gaussian mixture with K components. ![](https://render.githubusercontent.com/render/math?math=\overline{x}_i) reprents the empirical mean and ![](https://render.githubusercontent.com/render/math?math=s^2_i) variance.

<div class="image-wrapper" >
    <img src="/figures/empstatistics.png" alt=""/>
  </a>
      <p class="image-caption">Figure 2: Empirical statistics model.</p>
</div>
In a simulation case, fitted parameters need to be evaluated with true parameters which are known. Based on the law of empirical mean and variance of a univariate gaussian case, the fitted mean of the EM algorithm applied on S is able to provide accurate estimation of the true mean and variance parameters of X. In a bivariate case, the fitted mean vector has a same behavior as in univariate dimension where the true covariance matrix is compared to the estimated covariance matrix linked to the empirical mean vector and multiplied by the ensemble size. 

## Super sample: 
The simplest way to adapt the EM algorithm to ensemble data and take advantage of the large number of observations is to consider the ensemble as a super sample of ![](https://render.githubusercontent.com/render/math?math=(x_{11},\cdots,x_{nM})) of ![](https://render.githubusercontent.com/render/math?math=n{\times}M) independent realisations of the gaussian mixture.

<div class="image-wrapper" >
    <img src="/figures/supersample.png" alt=""/>
  </a>
      <p class="image-caption">Figure 3: Super sample model.</p>
</div>
To be able to predict a specific class for a new entering ensemble, a majority vote procedure is performed on the ensemble realisation.

## Exchangeable variable
 The alternative to the super sample approach is to consider the ensemble as a vector of variables ![](https://render.githubusercontent.com/render/math?math=(X_{1},\cdots,X_{M})) where ![](https://render.githubusercontent.com/render/math?math=X_{m}) is the m-th member and ![](https://render.githubusercontent.com/render/math?math=X_{m}) is assumed to be independent to ![](https://render.githubusercontent.com/render/math?math=X_{\ell}) if ![](https://render.githubusercontent.com/render/math?math=m\neq\ell) and exchangeable. A sequence of random variables is exchangeable
if its joint distribution is invariant under variable permutations. Then, the Gaussian mixture model framework is applied on the joint distribution between the discrete latent variable Z and the exchangeable sequence ![](https://render.githubusercontent.com/render/math?math=(X_{1},\cdots,X_{M})).

<div class="image-wrapper" >
    <img src="/figures/exchvariables.png" alt=""/>
  </a>
      <p class="image-caption">Figure 4: Exchangeable variables model.</p>
</div>


## Experimental setup

Two cases with K=4 clusters are considered with two different variable dimension (d=1 and d=3). One of them is referred to as "regular" because it is similar as examples of the literature. The marginal conditional distributions are plotted on the left panel of Figure 5. The clusters have distinct means and they are slightly overlapping. The second example is referred to as "difficult".  It is illustrated on the right panel of Figure 5. The clusters present a strong overlap. Two of them  have the same mean and different variances. Such a situation occurs in ensemble calibration problems when the ensembles are unbiased but  over (resp. under) dispersed. 

<div class="image-wrapper" >
    <img src="/figures/Simulation.png" alt=""/>
  </a>
      <p class="image-caption">Figure 5: Univariate case distribution.</p>
</div>

Gaussian component parameters are summarised in the table 1 for each case and gets same class proportion parameter.
  
<div class="image-wrapper" >
    <img src="/figures/table1.PNG" alt=""/>
</div>


## Results

The simulation implemented in the script GMMmain reproduce results displayed in the article for ab ensemble data set of size n=200 and ensemble size M=50. Each data set are generated 30 times by case and variable dimension. Threrefore, the three models are fitted on the simulated ensemble data set for different value of ![](https://render.githubusercontent.com/render/math?math=K+\in\\{2,...,6\\}). At the end, scores of RMSE (value close to zero means accurate estimation of the true parameters), accuracy (value going to one shows a good recovery of the true class serie Z by the model) and percent of selection of the number of classes K are computed and displayed below.

### Regular
<div class="image-wrapper" >
    <img src="/figures/Regular_RMSE.png" alt=""/>
  </a>
      <p class="image-caption">Figure 6: Estimated RMSE by models.</p>
</div>
<div class="image-wrapper" >
    <img src="/figures/Regular_ACC.png" alt=""/>
  </a>
      <p class="image-caption">Figure 7: Estimated Accuracy by models.</p>
</div>
<div class="image-wrapper" >
    <img src="/figures/Regular_BIC.png" alt=""/>
  </a>
      <p class="image-caption">Figure 8: Percent of the classes number K selected by models with the BIC score.</p>
</div>

### Difficult
<div class="image-wrapper" >
    <img src="/figures/Difficult_RMSE.png" alt=""/>
  </a>
      <p class="image-caption">Figure 9: Estimated RMSE by models.</p>
</div>
<div class="image-wrapper" >
    <img src="/figures/Difficult_ACC.png" alt=""/>
  </a>
      <p class="image-caption">Figure 10: Estimated Accuracy by models.</p>
</div>
<div class="image-wrapper" >
    <img src="/figures/Difficult_BIC.png" alt=""/>
  </a>
      <p class="image-caption">Figure 11: Percent of the classes number K selected by models with the BIC score.</p>
</div>
